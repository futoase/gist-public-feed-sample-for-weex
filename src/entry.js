import App from './App.vue';
import router from './router';
import { sync } from 'vuex-router-sync';

//sync(store, router)
import Paginate from 'vuejs-paginate';

Vue.use(Paginate);
Vue.component('paginate', Paginate);

new Vue(Vue.util.extend({
  el: '#root',
  router,
  method: {
    clickCallback: function(pageNum) {
      console.log(pageNum);
    }
  }
}, App));

router.push('/');
