import Router from 'vue-router'
import IndexView from './views/IndexView.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', component: IndexView }
  ]
});
